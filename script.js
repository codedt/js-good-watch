const	container = document.getElementById('container'),
			pausetimesTable = document.getElementById('id-pauseTimes'),
			infoField = document.getElementsByClassName('infoFont')[0],
			fieldNumber = document.getElementById('id-number'),
			btnpredefined = document.getElementById('id-btnpredefined'),
			addpredefValue = document.getElementById('id-addpredefvalue'),
			addpredef = document.getElementById('id-unit-addpredef'),
			addpredefDesc = document.getElementById('id-addpredefdesc'),
			MAXDAYSEXCEED = 1000,
			FORMATMIXEDTIME = 'd hh:mm:ss',
			TYPEOPTIONS = {
				'DAYS': 'days',
				'HOURS': 'hours',
				'MINUTES': 'minutes',
				'SECONDS': 'seconds',
				'MIXED': 'mixed'
			},
			MODES = {
				'NORMAL': 0,
				'PREDEF_ADDING': 1
			},
			REGEX_MIXED_FORMAT = new RegExp('^([0-9]|[1-9][0-9]|[1-9][0-9][0-9])\\s([0-1]?[0-9]|2[0-3]):[0-5]?[0-9]:[0-5]?[0-9]$');

var clockInterval = 0,
		infoTimeout = [],   // that info-messages will be displayed the appropriate time
		pauseTimes = [],
		customPredefs = [],  // value, timeType, desc
		pauseStart = 0,
		pauseEnd = 0,
		breakTime = 0,
		predefHoverCount = 0,
		infoDisplayCount = 0,
		nextPredefDivNum = 1,
		pauseOn = false,
		pauseTimes = [],
        currentMode = MODES.NORMAL;

function init() {
	document.getElementById('id-play').onclick = function () {
		pauseOn = false;
	}
	document.getElementById('id-pause').onclick = function () {
		pauseOn = true;
	}
	document.getElementById('id-export').onclick = function () {
		if (currentMode === MODES.NORMAL) {
			exportTimes();
		}
	}
	btnpredefined.onmouseover = function () {
		if (currentMode === MODES.NORMAL) {
			++predefHoverCount;
			$("#id-predefineds > *").fadeIn();
		} else {
			infoGui('Finish predef adding first');
		}
	}
	btnpredefined.onmouseout = function () {
		setTimeout(function() {
		--predefHoverCount;
			if (predefHoverCount <= 0) {
				$("#id-predefineds > *").fadeOut();
			}
		}, 2500);
	}
	document.getElementById('id-addnew').onclick = function () {
		if ( $(this).text() === 'Add Predefined') {
			startAddPrefMode();
		} else {
			finishAddPrefMode();
		}
	}

	document.getElementById('id-addpredef').onclick = function () {
		addNewPredef();
	}

	function adjustValuePlaceholder(p_targetValue, p_fieldNumber) {
		if (p_targetValue === TYPEOPTIONS.MIXED) {
			p_fieldNumber.placeholder = FORMATMIXEDTIME;
		} else {
			p_fieldNumber.placeholder = 'time value';
		}
	}
		
	function proposePredefDescription() {
		addpredefDesc.value = 
			addpredefValue.value + " " 
			+ addpredef.value.replace('hours', 'hrs')
			.replace('minutes', 'min')
			.replace('seconds', 'sec')
			.replace('mixed', '');
	}

	addpredefValue.onchange = function (event) {
		proposePredefDescription();
	}
	addpredef.onchange = function (event) {
		proposePredefDescription();
		adjustValuePlaceholder(
			event.target.value, 
			addpredefValue
		);
	}	
	document.getElementById('id-unit').onchange = function (event) {
		adjustValuePlaceholder(
			event.target.value, 
			document.getElementById('id-number')
		);
	}
}

function infoGui(msg) {
	++infoDisplayCount;
	infoField.innerHTML = msg;
	$('.infoFont').fadeIn();
	infoTimeout.push( setTimeout(function() {
		--infoDisplayCount;
		if (infoDisplayCount <= 0) {
			$('.infoFont').fadeOut();
			setTimeout(function() {
				clearInfoField();
			},1000);
		}
	}, 3000) );
}

function clearInfoField() {
	infoField.innerHTML = '';
	infoDisplayCount = 0;
	infoTimeout.forEach(function(e) {
		clearTimeout(e);
	});
	infoTimeout.splice(0);
}

function getMaximumTimeExceeded() {
	return calculateMS(MAXDAYSEXCEED, 'days');
}

function getInitialCountdownMS() { 
	let customNum = new Date(
								((	1 	 // Anzahl Tage
								* 1   // fuer Stunden
								* 1   // fuer Minuten
								* 10   // fuer Sek
								)+ 00
								)
								* 1000 // fuer Milisekunden
								); 
	return new Date(customNum).getTime();
}

function getTimeShowable(p_timeFormatted) {
	return p_timeFormatted.days+' '+ 
			('0'+p_timeFormatted.hours).slice(-2)+':'+ 
			('0'+p_timeFormatted.minutes).slice(-2)+':'+ 
			('0'+p_timeFormatted.seconds).slice(-2);
}

function calculateMS(value, unit) {
	let ms;

	switch (unit) {
		case 'days':
			ms = value * 1000 * 60 * 60 * 24;
			break;
		case 'hours':
			ms = value * 1000 * 60 * 60;
			break;
		case 'minutes':
			ms = value * 1000 * 60;
			break;
		case 'seconds':
			ms = value * 1000;
			break;
		case 'mixed':   // d hh:mm:ss
			ms = calculateMS(value.split(' ')[0], 'days')
				+ calculateMS(value.split(' ')[1].split(':')[0], 'hours')
				+ calculateMS(value.split(' ')[1].split(':')[1], 'minutes')
				+ calculateMS(value.split(' ')[1].split(':')[2], 'seconds');
			break;
	}
	return ms;
}

function prepareTimeToEnd(p_distance) {	
	return {
		'days': Math.floor(p_distance / (1000 * 60 * 60 * 24)),
		'hours': Math.floor((p_distance % (1000 * 60 * 60 * 24)) / (1000 * 60 * 60)),
		'minutes': Math.floor((p_distance % (1000 * 60 * 60)) / (1000 * 60)),
		'seconds': Math.floor((p_distance % (1000 * 60)) / 1000)
	};
}

function startClock(countDowndate) {
	const clock = document.getElementById('id-clockdiv'),
				daysSpan = clock.querySelector('.days'),
				hoursSpan = clock.querySelector('.hours'),
				minutesSpan = clock.querySelector('.minutes'),
				secondsSpan = clock.querySelector('.seconds'),
				nut = document.getElementById('id-nut');

	let distanceToFinish = 0;

	function finishClock() {
		container.classList.add('finish');
		nut.classList.add('finish');
		daysSpan.classList.add('finish');
		hoursSpan.classList.add('finish');
		minutesSpan.classList.add('finish');
		secondsSpan.classList.add('finish');
	}

	function prepareClockForNewRun() {
		container.classList.remove('finish');
		nut.classList.remove('finish');
		daysSpan.classList.remove('finish');
		hoursSpan.classList.remove('finish');
		minutesSpan.classList.remove('finish');
		secondsSpan.classList.remove('finish');
	}

	function showTimeOnClock(endTime) { 
		daysSpan.innerHTML = endTime.days;
		hoursSpan.innerHTML = ('0' + endTime.hours).slice(-2);
		minutesSpan.innerHTML = ('0' + endTime.minutes).slice(-2);
		secondsSpan.innerHTML = ('0' + endTime.seconds).slice(-2); 

		if (endTime.days <= 0
				&& endTime.hours <= 0
				&& endTime.minutes <= 0
				&& endTime.seconds <= 0
			) {
			clearInterval(clockInterval);
			finishClock();
		}
	}

	function addPauseTimeToTable(time, lnNumber) {
		let newCol, 
				newLine = document.createElement('tr'),
				timeFormatted;

		newCol = document.createElement('td');
		newCol.appendChild(document.createTextNode(lnNumber + ' -- '));
		newLine.appendChild(newCol);

		timeFormatted = prepareTimeToEnd(time);
		newCol = document.createElement('td');
		newCol.appendChild(document.createTextNode( 
			getTimeShowable(timeFormatted)
		));
		newLine.appendChild(newCol);
		pausetimesTable.appendChild(newLine);
	}

	function getCurrentTimeMS() {
		return Math.ceil(new Date().getTime() / 1000) * 1000;
	}

	function earsePauseInformation() {
		pauseOn = false;
		pauseStart = 0;
		breakTime = 0;
		while (pausetimesTable.hasChildNodes()) {
			pausetimesTable.removeChild(pausetimesTable.firstChild);
		}
		pauseTimes = [];
	}

	clearInfoField();
	if (countDowndate < getMaximumTimeExceeded()) {				// Check: maximal Time for clock
		showTimeOnClock( prepareTimeToEnd(countDowndate)); 	// immediate show-up
		countDowndate = countDowndate + getCurrentTimeMS();
		clearInterval(clockInterval);
		prepareClockForNewRun();
		earsePauseInformation();

		clockInterval = setInterval(function() {
			if (pauseOn == true && pauseStart == 0) { // check: pause on
					pauseStart = getCurrentTimeMS();
					pauseTimes.push(distanceToFinish);
					addPauseTimeToTable(distanceToFinish, pauseTimes.length);
			} else if (pauseOn == false) {  // in else area: after resuming check: if pausedcounttime > 0 and add it
				if (pauseStart > 0) { // after resuming immediately after pause
					breakTime = getCurrentTimeMS() - pauseStart;
					pauseStart = 0;
				}
				if (breakTime > 0) { // check: was there are break 
					countDowndate = countDowndate + breakTime;
					breakTime = 0;
				}
				distanceToFinish = countDowndate - getCurrentTimeMS();				
				showTimeOnClock( prepareTimeToEnd(distanceToFinish));
			}
		}, 1000);
	} else {
		infoGui('Maximal time exceeded');
	}

}


function exportTimes() {
	let csvData = 'Pause-Times\n',
	    hiddenElement,
	    pauseTimesPrepared = [];

	if (pauseTimes.length > 0) {
		for (var i = 0; i < pauseTimes.length; i++) {
			pauseTimesPrepared[i] = getTimeShowable( prepareTimeToEnd( pauseTimes[i]));
		};
	  pauseTimesPrepared.forEach(function(row) {
	  	csvData += row + ',';
	  });
	  csvData = csvData.substring(0, csvData.length - 1);

		hiddenElement = document.createElement('a');
		hiddenElement.href = 'data:text/csv;charset=utf-8,' + encodeURI(csvData);
		hiddenElement.target = '_blank';
		hiddenElement.download = 'times.csv';
		hiddenElement.click();
	} else {
		infoGui('Nothing to export');
	}
}

function startAddPrefMode() {
	currentMode = MODES.PREDEF_ADDING;

	// show fields for adding
	$("#id-divaddingpredefs").fadeIn();
	$("#id-addnew").text('Cancel Predef.');  
}

function finishAddPrefMode() {
	$("#id-divaddingpredefs").fadeOut();
	$("#id-addnew").text('Add Predefined');  
	$("#id-addpredefvalue").val("");
	$("#id-addpredefdesc").val("");

	// enable the buttons and modes
	currentMode = MODES.NORMAL;
}

function addPredefToGUI(value, timeType, desc) {
	let currentDivNum = Math.floor((customPredefs.length-1) / 4) + 1,
			div_customPredefs = document.getElementById('id-divcustompredefs'+currentDivNum),
			elem,
			div_elem;

	if ((customPredefs.length % 4) === 0) {  // neues div anlegen
		div_elem = document.createElement('div');
		div_elem.id = 'id-divcustompredefs'+(++nextPredefDivNum);
		div_elem.setAttribute('class', 'paragraph btn-margin-bottom-sm btn-format hide');
		document.getElementById('id-predefineds').appendChild(div_elem);
	}
		elem = document.createElement('a');
		elem.type = 'button';
		elem.setAttribute("value", (calculateMS(value, timeType) / 1000));
		elem.setAttribute("onclick", "startVal(this)");
		elem.innerText = desc;
		div_customPredefs.appendChild(elem);
}

function addNewPredef() {
	let value = addpredefValue.value,
			unit = document.getElementById('id-unit-addpredef').value,
			desc = addpredefDesc.value;

	// validation
	if (value.length > 0 && desc.length > 0) {	  
		if ( (unit !== 'mixed' && isNaN(value) === false) 
				|| (unit === 'mixed' && REGEX_MIXED_FORMAT.test(value)) ) {
			customPredefs.push([(value), unit, desc]); 
			addPredefToGUI(value, unit, desc);
			finishAddPrefMode();
		} else {
			infoGui('Wrong format ('+FORMATMIXEDTIME+')');
		}
	} else {
		value.length > 0 
			? infoGui('Empty description field')
			: infoGui('Empty value field');
	}
	// back to the flow ???
}

function startVal(t) {
	startClock(t.getAttribute('value')*1000);
}

function startManual(t) {
	const value = t.parentNode.querySelector('#id-number').value,
				unit = t.parentNode.querySelector('#id-unit').value;

	if (value.length > 0) {
		if ( (unit !== 'mixed' && isNaN(value) === false) 
			|| (unit === 'mixed' && REGEX_MIXED_FORMAT.test(value)) ) {
			startClock( calculateMS(value, unit));
		} else {
			infoGui('Wrong format ('+FORMATMIXEDTIME+')');
		}
	} else {
		infoGui('Empty field');
	}
}

document.addEventListener('DOMContentLoaded', function() {
	init();
	startClock(getInitialCountdownMS());

	$("id-predefineds > *").each(function() {
		$( this ).addClass(".hide");
	});
}, false);