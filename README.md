# README #

To start the application run "index.html". 

### What is this repository for? ###

* Quick summary

This application allows you to set the time in days, hours, minutes and seconds. And it will automatically count the time down. When the times runs out some parts of the clock will change to red. 

You can set the time from the predefined options in the customized drop-down box or set a time manually in the third line below the clock. 

The 'Add Predefined' mode let you define an customized time value for the clock which you can later find in the 'Predefined' dropdown. 

At any time you can pause the time and also continue. The pause times will be listed below. The pause times can then be exported to an csv file. 

* Version

V1.0